﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RandomNumber
{
    class Program
    {
        static List<string> players = new List<string>();

        static int minNum = 12;
        static int maxNum = 121;
        static int gameNumber = new Random().Next(minNum, maxNum);
        static int minPlayerInput = 1;
        static int maxPlayerInput = 4;

        static void GetPlayers(bool countPlayers)
        {
            players.Clear();

            if (countPlayers)
            {
                while (true)
                {
                    Console.WriteLine("Введите имя игрока или 0 для отмены:");
                    string user = Console.ReadLine();
                    if (user != "0")
                        players.Add(user);
                    else
                        break;
                }
            }
            else
            {
                Console.WriteLine("Введите свое имя:");
                players.Add(Console.ReadLine());
                players.Add("Steve");
            }

            Console.WriteLine("Игроки добавлены!");
        }

        static void SetNewGameParams(char gameParams)
        {
            while (true)
            {
                Console.WriteLine("Введите 2 числа. Сначала минимальное, затем максимальное: ");

                string minInput = Console.ReadLine();
                string maxInput = Console.ReadLine();

                if (int.TryParse(minInput, out int resultMin) && int.TryParse(maxInput, out int resultMax))
                {

                    minNum = resultMin;
                    maxNum = resultMax;

                    if ((maxNum > minNum) && (gameParams == '3'))
                    {
                        gameNumber = new Random().Next(minNum, maxNum + 1);
                        Console.WriteLine("Игровое число изменено!");
                        break;
                    }
                    else if ((maxNum > minNum) && (gameParams == '4'))
                    {
                        minPlayerInput = minNum;
                        maxPlayerInput = maxNum;
                        Console.WriteLine("Доступный диапазон изменен!");
                        break;
                    }
                    else
                        Console.WriteLine("Максимальное число должно быть больше минимального!\nПопробуйте снова!");
                }
                else
                {
                    Console.WriteLine("Неверный ввод! Можно вводить только числа!");
                }
            }
        }

        static int PlayerStep()
        {
            while (true)
            {
                string input = Console.ReadLine();

                if (int.TryParse(input, out int result))
                {
                    if((result <= maxPlayerInput) && (result >= minPlayerInput))
                        return result;
                    else
                        Console.WriteLine("Неверный ввод! Можно вводить только числа из диапазона - {0}:{1}", minPlayerInput, maxPlayerInput);
                }
                else
                    Console.WriteLine("Неверный ввод! Можно вводить только числа!");
            }
        }

        static bool Winner(string player)
        {
            Console.WriteLine("Игрок {0} победил!\nХотите сыграть снова (1 - Да, любой ввод - Нет)?", player);

            if (Console.ReadLine() == "1")
            {
                gameNumber = new Random().Next(minNum, maxNum);
                Console.WriteLine("Игра началась! Игровое число: {0}", gameNumber);

                return true;
            }
            else
                return false;
        }

        static void PrintMenu()
        {
            bool isAlive = true;
            while (isAlive)
            {
                Console.WriteLine("Для начала игры введи номер нужной операции:\n" +
                    "1. Одиночная игра\n2. Играть c друзьями\n3. Изменить доступный диапазон для ввода\n4. Изменить игровое число\n5. Правила\n6. Выход");
                string playerInput = Console.ReadLine();

                switch (playerInput)
                {
                    case "1":
                        SinglePlay();
                        break;

                    case "2":
                        MultiPlay();
                        break;

                    case "3":
                        SetNewGameParams('4');
                        break;

                    case "4":
                        SetNewGameParams('3');
                        break;

                    case "5":
                        Console.WriteLine("Игрок должен поочередно (с компьютером или другими игроками) вводить число\n " +
                            "из заданного диапазона. По умолчанию диапазоном считается 1:4. Игрок должен стремиться\n " +
                            "вводить числа так, что бы при вычитании его числа из Игрового числа, Игровое число стало нулем\n " +
                            "или меньше. Игровое число генерируется рандомно из заданного диапазона. По умолчнию диапазон от\n " +
                            "12 до 120 включительно.\n");
                        break;

                    case "6":
                        isAlive = false;
                        break;
                }
            }
        }

        static void MultiPlay()
        {
            GetPlayers(true);
            Console.WriteLine("Игра началась! Игровое число: {0}", gameNumber);

            bool isPlay = true;
            while (isPlay)
            {

                foreach (string player in players)
                {
                    Console.WriteLine("\nХод игрока {0}", player);

                    int playerStep = PlayerStep(); 

                    Console.WriteLine("\nИгрок {0} ввел число {1}\nИгровое число теперь: {2}",
                                            player, playerStep, gameNumber -= playerStep);

                    if (gameNumber <= 0)
                    {
                        isPlay = Winner(player);
                        break;
                    }
                }
            }
        }

        static void SinglePlay()
        {
            GetPlayers(false);
            Console.WriteLine("Игра началась! Игровое число: {0}\nТекущий диапазон: {1}:{2}", gameNumber, minPlayerInput, maxPlayerInput);
            
            int playerStep = 0;
            bool isPlay = true;
            while (isPlay)
            {
                foreach (string player in players)
                {

                    Console.WriteLine("\nХод игрока {0}", player);

                    if (player == "Steve")
                        playerStep = new Random().Next(minPlayerInput, maxPlayerInput + 1);
                    else
                        playerStep = PlayerStep();

                    Console.WriteLine("\nИгрок {0} ввел число {1}\nИгровое число теперь: {2}",
                                            player, playerStep, gameNumber -= playerStep);

                    if (gameNumber <= 0)
                    {
                        isPlay = Winner(player);
                        break;
                    }
                }
            }
        }


        static void Main(string[] args)
        {

            PrintMenu();
        }
    }
}
